// 请求服务端接口 动作名称
const ACTION = {
	ADD: 'add',
	DEL: 'del',
	EDIT: 'edit',
	QUERY: 'query',	
	LOGIN: 'login',
	LOGOUT: 'logout',
	LOGIN_BY_SMS: 'loginBySms',
	BIND_MOBILE: 'bindMobile',
	WX_LOGIN: 'wxLogin',
	REGISTER: 'register',
	QUERY_ONE: 'queryOne',
	QUERY_TWO: 'queryTwo',
	QUERY_THREE: 'queryThree',
	QUERY_BY_ID: 'queryById',
	CHECK_TOKEN: 'checkToken',
	EDIT_BY_NUM: 'editByNum',
	EDIT_BY_NAME: 'editByName',
	QUERY_BY_NAME: 'queryByName',
	QUERY_BY_TIME: 'queryByTime',
	SEND_SMS_CODE: 'sendSmsCode',
	VERIFY_CODE: 'verifyCode',
	GET_USER_INFO: 'getUserInfo'
}

//请求服务端接口  云函数名 后端back 接口
const FUNC_NAME = {
	B_USER: 'b_user',
	B_PAGE: 'b_page',
	B_ORDER: 'b_order',
	B_GOODS: 'b_goods',
	B_SOURCE: 'b_source',
	B_TABBAR: 'b_tabbar',
	B_COUPON: 'b_coupon',
	B_QRCODE: 'b_qrcode',
	B_SETTING: 'b_setting',
	B_ADDRESS: 'b_address',
	B_SELFGET: 'b_selfget',
	B_COUPON_USER: 'b_coupon_user',
	B_GOODS_GROUP: 'b_goods_group',
	B_SELFGET_USER: 'b_selfget_user',
	B_SOURCE_GROUP: 'b_source_group',
	B_SHIPPING_TEMPLATE: 'b_shipping_template',
}

//请求服务端接口 返回状态码
const CODE = {
	SUCCESS: 200,
	TOEKN_INVALID: 401,
	OTHER_TIP: 402
}

module.exports = {
	ACTION,
	FUNC_NAME,
	CODE
}

