## zxshop
zxshop是一套基于uniapp+uniCloud开发的商城系统，目前已适配H5、微信小程序。支持移动端可视化设计，加强了用户体验。目前支持移动端后台管理，PC端后台管理还在开发的路上哦，保持关注吧！如果您对zxshop感兴趣，欢迎关注如下订阅号，最新的开发动态将在订阅号中实时发布。

#### 对比优势
* 可视化设计，模板可多样化，支持各行各业
* 支持移动端管理，升华管理员用户体验

#### 支持组件
* 基础组件：文本、图片广告、商品、视频、标题、图文导航、商品搜索、公告、辅助空白、辅助线、自由面板
* 系统组件：分类模板、页面导航
* 营销组件：优惠券

#### 商城体验（扫码关注公众号）
![微信小程序](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-zxshop/ac305550-e79a-11ea-b244-a9f5e5565f30.jpg)
* 回复：用户端小程序。即可体验用户端微信小程序
* 回复：管理端小程序。即可体验管理端微信小程序
* 回复：用户端H5。即可体验用户端H5页面
* 回复：管理端H5。即可体验管理端H5页面
* 管理端登录用户名和密码：admin 123456

#### zxshop交流微信群（广告误入）
![微信群](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-zxshop/7338a3f0-e7ff-11ea-8ff1-d5dcf8779628.png)

#### 找我开发小程序（定制化开发）
![微信小程序](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-zxshop/71b98a30-e7a0-11ea-8a36-ebb87efcf8c0.jpg)

### 前端部署
1. 前端配置文件 根目录config.js
2. 前端配置文件 cloudfunctions/common/config
4. 前端配置文件 cloudfunctions/common/uni-id

### 后端部署
1. 后端配置文件 根目录config.js

### 前端下载地址
[点击进入用户端下载页](https://ext.dcloud.net.cn/plugin?id=2620) 

### 后端下载地址
[点击进入管理端下载页](https://ext.dcloud.net.cn/plugin?id=2629) 