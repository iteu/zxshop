import request from '@/common/js/request'
import myConst from '@/common/js/const'
const url = myConst.URL.DASHBOARD
export const getShopData = (data) => {
	return request({
		url,
		check:true,
		action: myConst.ACTION.QUERY,
		data
	})
}
export const getTodoData = (data) => {
	return request({
		url,
		check:true,
		action: myConst.ACTION.QUERY_ONE,
		data
	})
}
export const getStaData = (data) => {
	return request({
		url,
		check:true,
		action: myConst.ACTION.QUERY_TWO,
		data
	})
}
export const getGoodsData = (data) => {
	return request({
		url,
		check:true,
		action: myConst.ACTION.QUERY_THREE,
		data
	})
}
