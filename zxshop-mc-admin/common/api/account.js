import request from '@/common/js/request'
import myConst from '@/common/js/const'
const url = myConst.URL.ACCOUNT
export const adminLogin = (data) => {
	return request({
		check: false,
		url,
		action: myConst.ACTION.ADMIN_LOGIN,
		data
	})
}
export const adminLogout = (data) => {
	return request({
		check: false,
		url,
		action: myConst.ACTION.LOGOUT,
		data
	})
}
export const updatePwd = (data) => {
	return request({
		check: true,
		url,
		action: myConst.ACTION.EDIT,
		data
	})
}